function textModalSuccess(title, text) {
    $('#exampleModalLong').modal();
    $("#exampleModalLongTitle").html(title);
    $("#modal_text").html(text);
}

function textModalFail(title, text) {
    $('#exampleModalLong').modal();
    $("#exampleModalLongTitle").html(title);
    $("#modal_text").html(text);
}

function definedCLient() {
    localStorage.setItem('tbClientes', []);
}

function Adicionar(mail) {
    console.log({"user_email": mail});
    localStorage.setItem("tbClientes", JSON.stringify({"user_email": mail}));
}

function buscarPerfil() {
    var tbClientes = localStorage.getItem("tbClientes");// Recupera os dados armazenados
    if (tbClientes) {
        var clientes = tbClientes; // Converte string para objeto
    }
    return clientes;
}


$(document).ready(function () {
    var user = buscarPerfil();
    if (user) {
        console.log("User:" + user);
        goToIndex();
    } else {
        definedCLient();
    }
    var btn = $('#btn_send');
    var btn_close = $('.btn-secondary');
    var load = $('.load-tag');
    var success = false;
    var ob;
    load.hide();
    btn.click(function () {
        validEmail();
    });

    function emailIsValid(email) {
        return /\S+@\S+\.\S+/.test(email);
    }

    function validEmail() {
        var email = $('#sample3').val();
        if (emailIsValid(email)) {
            ob = email;
            textModalSuccess("Success", "Click in next for contributing to our research.");
            success = true;
        } else {
            textModalFail("Sorry", "Email not correct.");
        }
        /** var text_email = $('.email').val();
         var myForm = document.getElementById("form-data-send");
         var formData = new FormData(myForm);
         if (text_email) {
            var request = new XMLHttpRequest();
                request.open("POST", "php/email-validate.php");
            request.onreadystatechange = function () {
                if (request.readyState == 4 && request.status == 200) {
                    ob = $.parseJSON(request.responseText);
                    if (ob['uf04_cod_expert']) {
                        success = true;
                        btn_close.html('Next');
                        textModalSuccess("Success", "Click in next.");
                    } else {
                        textModalFail("Sorry", "E-mail not found.");
                    }
                } else if (request.status != 200) {
                    textModalFail("Pay Attention", 'Your answer was not sent.');
                }
            }
            request.send(formData);
        } else {
            textModalFail("Pay attention", "Your e-mail are empty.");
        }
         **/
    }

    btn_close.click(function () {
        if (success) {
            Adicionar(ob);
            window.location.replace("./forms-send.php");
        }
    });

    function goToIndex() {
        window.location.replace("./forms-send.php");
    }

});


