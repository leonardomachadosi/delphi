$(document).ready(function () {
    var btShowModalfunctional = $('.show-modal-functional');
    var btShowModalReliability = $('.show-modal-reliability');
    var btShowModalEfficiency = $('.show-modal-efficiency');
    var btShowModalOperability = $('.show-modal-operability');
    var btShowModalCompatibility = $('.show-modal-compatibility');
    var btShowModalSecurity = $('.show-modal-security');
    var btShowModalMaintainability = $('.show-modal-maintainability');
    var btShowModalTransferability = $('.show-modal-transferability');
    var btShowModalEpic = $('.show-modal-epic');
    var btShowModalContext = $('.show-modal-context');
    var btShowModalMobility = $('.show-modal-mobility');
    var btShowModalAttention = $('.show-modal-attention');
    var btShowModalCalmness = $('.show-modal-calmness');
    var btShowModalTransparency = $('.show-modal-transparency');

    var bt_close = $('.close');

    btShowModalfunctional.click(function () {
        $('#modal_dialog_functional').modal();
    });

    btShowModalReliability.click(function () {
        $('#modal_dialog_reliability').modal();
    });
    btShowModalEfficiency.click(function () {
        $('#modal_dialog_efficiency').modal();
    });

    btShowModalOperability.click(function () {
        $('#modal_dialog_operability').modal();
    });

    btShowModalCompatibility.click(function () {
        $('#modal_dialog_compatibility').modal();
    });

    btShowModalSecurity.click(function () {
        $('#modal_dialog_security').modal();
    });

    btShowModalMaintainability.click(function () {
        $('#modal_dialog_maintainability').modal();
    });
    btShowModalTransferability.click(function () {
        $('#modal_dialog_transferability').modal();
    });

    btShowModalEpic.click(function () {
        $('#modal_dialog_epic').modal();
    });

    btShowModalContext.click(function () {
        $('#modal_dialog_context').modal();
    });

    btShowModalMobility.click(function () {
        $('#modal_dialog_mobility').modal();
    });

    btShowModalAttention.click(function () {
        $('#modal_dialog_attention').modal();
    });

    btShowModalCalmness.click(function () {
        $('#modal_dialog_calmness').modal();
    });
    btShowModalTransparency.click(function () {
        $('#modal_dialog_transparency').modal();
    });

});


function w3_open() {
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}

function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}

