function textModal(title, text) {
    $('#modal_dialog_success').modal();
    $("#texto_modal").html(text);
    $("#h_title").html(title);
}

function goToIndex() {
    localStorage.clear();
    window.location.replace("./index.html");
}

function adicionar(id) {
    localStorage.setItem("expert", JSON.stringify({"expert_id": id}));
}

function buscarPerfil() {
    var tbClientes = localStorage.getItem("expert");// Recupera os dados armazenados
    if (tbClientes) {
        var clientes = tbClientes; // Converte string para objeto
    }
    return clientes;
}


function checkFs(response, code, c, characterisitic) {
    var text = '';
    var text_owner = '';
    if (response[code]['fkuf09uf03_cod_qualifier'] === '3') {
        document.getElementById(c + '_options_3').checked = true;
        text_owner = 'Very relevant';
    } else if (response[code]['fkuf09uf03_cod_qualifier'] === '2') {
        document.getElementById(c + '_options_2').checked = true;
        text_owner = 'Relevant';
    } else if (response[code]['fkuf09uf03_cod_qualifier'] === '1') {
        document.getElementById(c + '_options_1').checked = true;
        text_owner = 'Not relevant';
    }

    if (response[code]['uf09_score_first_round'] === '3') {
        text = 'Very relevant';
    } else if (response[code]['uf09_score_first_round'] === '2') {
        text = 'Relevant';
    } else if (response[code]['uf09_score_first_round'] === '1') {
        text = 'Not relevant';
    }

    secondeScode(response, code, c, 'two');
    thirdScore(response, code, c, 'three');

    document.getElementById(c + '_percent').textContent = response[code]['uf09_percent_first_round'] + '%';
    document.getElementById(c + '_text').textContent = text;
    document.getElementById(c + '_owner').textContent = text_owner;
    document.getElementById(c + '_name').textContent = characterisitic;
}

function secondeScode(response, code, c, suf) {
    var text = '';
    if (response[code]['uf09_score_first_round_two'] === '3') {
        text = 'Very relevant';
    } else if (response[code]['uf09_score_first_round_two'] === '2') {
        text = 'Relevant';
    } else if (response[code]['uf09_score_first_round_two'] === '1') {
        text = 'Not relevant';
    }

    document.getElementById(c + '_percent_' + suf).textContent = response[code]['uf09_percent_first_two'] + '%';
    document.getElementById(c + '_text_' + suf).textContent = text;
}

function thirdScore(response, code, c, suf) {
    var text = '';
    if (response[code]['uf09_score_first_round_three'] === '3') {
        text = 'Very relevant';
    } else if (response[code]['uf09_score_first_round_three'] === '2') {
        text = 'Relevant';
    } else if (response[code]['uf09_score_first_round_three'] === '1') {
        text = 'Not relevant';
    }

    document.getElementById(c + '_percent_' + suf).textContent = response[code]['uf09_percent_first_three'] + '%';
    document.getElementById(c + '_text_' + suf).textContent = text;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function setCheck(response) {

    checkFs(response, 1, 'fs', 'Functional Suitability');
    checkFs(response, 2, 're', 'Reliability');
    checkFs(response, 3, 'pe', 'Performance efficiency');
    checkFs(response, 4, 'op', 'Operability');
    checkFs(response, 5, 'se', 'Security');
    checkFs(response, 6, 'co', 'Compatibility');
    checkFs(response, 7, 'ma', 'Maintainability');
    checkFs(response, 8, 'tf', 'Transferability');
    checkFs(response, 9, 'ec', 'EPI.C');
    checkFs(response, 10, 'ca', 'Context-awareness');
    checkFs(response, 11, 'mo', 'Mobility');
    checkFs(response, 12, 'at', 'Attention');
    checkFs(response, 13, 'cm', 'Calmness');
    checkFs(response, 14, 'tr', 'Transparency');

}

$(document).ready(function () {
    // var expert = buscarPerfil();
    var btn_close = $('.btn-secondary');
    var load = $('.load-tag');
    var btn = $('#btn_send');
    var success = false;

    const param = getUrlVars()["code"];
    const idx = buscarPerfil();
    if (!param && !idx) {
        console.log('chegou aqui');
        goToIndex();
    } else {
        var mailExpert = null;
        if (param) {
            mailExpert = param;
        } else {
            mailExpert = idx;
        }
        adicionar(mailExpert);
        $('#expert_id').attr('value', mailExpert);
        fillFields(mailExpert);
    }
    load.hide();
    btn.click(function () {
        sendData();
    });

    function sendData() {
        var data = $('.form-data-send').serialize();
        if (data) {
            load.show();
            btn.hide();
            var dataJson = JSON.parse('{"' + decodeURI(data).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}')
            var formData = new FormData();
            formData.append("expertId", mailExpert);
            formData.append("fs_options", dataJson['fs_options']);
            formData.append("fs_text_option", dataJson['fs_text_option']);
            formData.append("re_options", dataJson['re_options']);
            formData.append("re_text_option", dataJson['re_text_option']);
            formData.append("pe_options", dataJson['pe_options']);
            formData.append("pe_text_option", dataJson['pe_text_option']);
            formData.append("op_options", dataJson['op_options']);
            formData.append("op_text_option", dataJson['op_text_option']);
            formData.append("se_options", dataJson['se_options']);
            formData.append("se_text_option", dataJson['se_text_option']);
            formData.append("co_options", dataJson['co_options']);
            formData.append("co_text_option", dataJson['co_text_option']);
            formData.append("ma_options", dataJson['ma_options']);
            formData.append("ma_text_option", dataJson['ma_text_option']);
            formData.append("tf_options", dataJson['tf_options']);
            formData.append("tf_text_option", dataJson['tf_text_option']);
            formData.append("ec_options", dataJson['ec_options']);
            formData.append("ec_text_option", dataJson['ec_text_option']);
            formData.append("ca_options", dataJson['ca_options']);
            formData.append("ca_text_option", dataJson['ca_text_option']);
            formData.append("mo_options", dataJson['mo_options']);
            formData.append("mo_text_option", dataJson['mo_text_option']);
            formData.append("at_options", dataJson['at_options']);
            formData.append("at_text_option", dataJson['at_text_option']);
            formData.append("cm_options", dataJson['cm_options']);
            formData.append("cm_text_option", dataJson['cm_text_option']);
            formData.append("tr_options", dataJson['tr_options']);
            formData.append("tr_text_option", dataJson['tr_text_option']);
            var request = new XMLHttpRequest();
            request.open('POST', 'php/validated-form.php');
            request.onreadystatechange = function () {
                if (request.readyState == 4 && request.status == 200) {
                    if (request.responseText == 0) {
                        textModal("Pay Attention", "There are empty fields.");
                        btn.show();
                        load.hide();
                    } else if (request.responseText == 1) {
                        textModal("Thanks.", "Your answer was sent successfully. Thanks for contributing to" +
                            " our research, your opinion will be very important in this process.");
                        success = true;
                        load.hide();
                    } else {
                        textModal("Pay Attention", 'Your answer was not sent.');
                        btn.show();
                        load.hide();
                    }
                } else if (request.status != 200) {
                    textModal("Pay Attention", 'Your answer was not sent.');
                    btn.show();
                    load.hide();
                }
            }
            request.send(formData);
        }

    }


    btn_close.click(function () {
        if (success) {
            goToIndex();
        }
    });


    function fillFields(mailExpert) {
        var form = new FormData();
        form.append("expertId", mailExpert);
        var request = new XMLHttpRequest();
        request.open('POST', 'php/get_answer.php');
        request.onreadystatechange = function () {
            try {
                if (request.readyState === 4 && request.status === 200) {
                    var response = JSON.parse(request.responseText);
                    if (response) {
                        setCheck(response);
                    } else {
                        console.log(request.readyState);
                        goToIndex();
                    }

                } else if (request.status !== 200) {
                    alert('Error');
                }
            } catch (err) {
                console.log(err);
                goToIndex();
            }
        }

        request.send(form);
    }

})
;