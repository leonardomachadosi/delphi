<!DOCTYPE html
        PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/html">
<title>Software Product Quality in Smart City Applications</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<script type="application/javascript" defer src="js/main-script.js"></script>
<script type="application/javascript" defer src="js/custom.js"></script>

<script src="https://code.jquery.com/jquery-3.4.1.js" type="text/javascript"></script>

<link rel="shortcut icon" type="image/png" href="img/lsdi_logo.png"/>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<body>

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-new w3-collapse w3-top w3-large w3-padding"
     style="z-index:3;width:300px;font-weight:bold; background: darkgray !important;"
     id="mySidebar"><br>
    <a href="javascript:void(0)" onclick="w3_close()" class="w3-button w3-hide-large w3-display-topleft"
       style="width:100%;font-size:22px"> X </a>
    <div class="w3-container">
        <a href="https://portais.ufma.br/PortalUfma/index.jsf"
           target="_blank"> <img src="./img/brasao-normal.png"
                                 width="150px"
                                 height="150px"
                                 alt=""
                                 style="cursor: pointer"/>
        </a>
        <div style="width: 205px;
        height: 205px;
          margin-top: 8px;">
            <a href="http://cidadesinteligentes.lsdi.ufma.br/doku.php"
               target="_blank">
                <img alt=""
                     src="./img/lsdi_logo.png"
                     width="200px"
                     height="150px"
                     style="cursor: pointer"/>
            </a>
        </div>
        <hr/>

    </div>
    <div class="w3-bar-block">
        <a href="index.html" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white"><b>Home</b></a>
        <a href="forms-send.php" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white"><b>Evaluation</b></a>
    </div>
</nav>

<!-- Top menu on small screens -->
<header class="w3-container w3-top w3-hide-large w3-new w3-xlarge w3-padding">
    <a href="javascript:void(0)" class="w3-button w3-new w3-margin-right" onclick="w3_open()">☰</a>
    <span>Software Product Quality in Smart City Applications</span>
</header>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu"
     id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:340px;margin-right:40px">

    <!-- Header -->
    <div class="w3-container" style="margin-top:80px" id="showcase">
        <h1 class="w3-jumbo"><b>Software Product Quality in Smart City Applications</b></h1>
        <br/>
        <br/>
        <h2 style="color: darkcyan"><b>Evaluate the characteristics below indicating the relevance of each to
                the context of Smart Cities applications</b></h2>
        <hr style="width:50px;border:5px solid darkcyan" class="w3-round">

        <br/>
        <br/>

        <form action="javascript:sendData();"
              enctype="multipart/form-data"
              method="post" class="form-data-send">
            <input name="expertId" id="expert_id" type="hidden"/>
        </form>

        <!-- Functional Suitability-->
        <form action="javascript:sendData();"
              enctype="multipart/form-data"
              id="form-data-send"
              method="post" class="form-data-send">

            <h3>
                <b>Functional Suitability</b>
                <i class="material-icons  mdl-list__item-avatar show-modal-functional new_icon" style="cursor: pointer">info</i>
            </h3>
            <br/>
            <h5>Notes:</h5>

            <h5>In the first round, for the <span id="fs_name"></span> quality characteristic, <b id="fs_percent"></b>
                of experts have
                considered "<b id="fs_text"></b>", <b id="fs_percent_two"></b> considered "<b id="fs_text_two"></b>",
                and <b id="fs_percent_three"></b> considered "<b id="fs_text_three"></b>" for the development of
                Smart City applications. Your opinion was "<b id="fs_owner"></b>". If you want to change your opinion,
                please select a new option below.
                <br/>
                <br/>
                <input type="radio" id="fs_options_1" name="fs_options" value="1">
                <label for="male"><h5>Not relevant</h5></label><br>
                <input type="radio" id="fs_options_2" name="fs_options" value="2">
                <label for="female"><h5>Relevant</h5></label><br>
                <input type="radio" id="fs_options_3" name="fs_options" value="3">
                <label for="other"><h5>Very relevant</h5></label>
                <br/>
                <h5>If you have changed your opinion, could you please justify your new choice? You can also make any
                    other comment, as you wish.</h5>
                <br/>
                <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="fs_text_option"></textarea>
                </div>
                <br/>
        </form>
        <hr/>
        <!-- End Functional Suitability -->


        <!-- Releability -->
        <form action="javascript:sendData();"
              class="form-data-send" method="post">
            <h3>
                <b> Reliability </b>
                <i class="material-icons  mdl-list__item-avatar show-modal-reliability new_icon"
                   style="cursor: pointer">info</i>
            </h3>
            <br/>
            <h5>Notes:</h5>
            <h5>In the first round, for the <span id="re_name"></span> quality characteristic, <b id="re_percent"></b>
                of experts have
                considered "<b id="re_text"></b>", <b id="re_percent_two"></b> considered "<b id="re_text_two"></b>",
                and <b id="re_percent_three"></b> considered "<b id="re_text_three"></b>" for the development of
                Smart City applications. Your opinion was "<b id="re_owner"></b>". If you want to change your opinion,
                please select a new option below.
                <br/>
                <br/>
                <input type="radio" id="re_options_1" name="re_options" value="1">
                <label for="male"><h5>Not relevant</h5></label><br>
                <input type="radio" id="re_options_2" name="re_options" value="2">
                <label for="female"><h5>Relevant</h5></label><br>
                <input type="radio" id="re_options_3" name="re_options" value="3">
                <label for="other"><h5>Very relevant</h5></label>
                <br/>
                <br/>
                <h5>If you have changed your opinion, could you please justify your new choice? You can also make any
                    other comment, as you wish.</h5>
                <br/>
                <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="re_text_option"></textarea>
                </div>
                <br/>
        </form>
        <hr/>

        <!-- End Releability -->

        <!-- Performance efficiency -->
        <form action="javascript:sendData();"
              class="form-data-send" method="post">
            <h3>
                <b>Performance efficiency</b>
                <i class="material-icons  mdl-list__item-avatar show-modal-efficiency new_icon"
                   style="cursor: pointer">info</i>
                <br/>

                <h5>Notes:</h5>
                <h5>In the first round, for the <span id="pe_name"></span> quality characteristic, <b
                            id="pe_percent"></b>
                    of experts have
                    considered "<b id="pe_text"></b>", <b id="pe_percent_two"></b> considered "<b id="pe_text_two"></b>",
                    and <b id="pe_percent_three"></b> considered "<b id="pe_text_three"></b>" for the development of
                    Smart City applications. Your opinion was "<b id="pe_owner"></b>". If you want to change your
                    opinion, please select a new option below.
                    <br/>
                    <br/>
                    <input type="radio" id="pe_options_1" name="pe_options" value="1">
                    <label for="male"><h5>Not relevant</h5></label><br>
                    <input type="radio" id="pe_options_2" name="pe_options" value="2">
                    <label for="female"><h5>Relevant</h5></label><br>
                    <input type="radio" id="pe_options_3" name="pe_options" value="3">
                    <label for="other"><h5>Very relevant</h5></label>
                    <br/>
                    <br/>
                    <h5>If you have changed your opinion, could you please justify your new choice? You can also
                        make
                        any
                        other comment, as you wish.</h5>
                    <br/>
                    <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="pe_text_option"></textarea>
                    </div>
                    <br/>

        </form>
        <hr/>

        <!-- End Performance efficiency -->

        <!-- Operability -->
        <form action="javascript:sendData();"
              class="form-data-send" method="post">
            <h3>
                <b>Operability</b>
                <i class="material-icons  mdl-list__item-avatar show-modal-operability new_icon"
                   style="cursor: pointer">info</i>
            </h3>
            <br/>
            <h5>Notes:</h5>
            <h5>In the first round, for the <span id="op_name"></span> quality characteristic, <b
                        id="op_percent"></b>
                of experts have
                considered "<b id="op_text"></b>", <b id="op_percent_two"></b> considered "<b id="op_text_two"></b>",
                and <b id="op_percent_three"></b> considered "<b id="op_text_three"></b>" for the development of
                Smart City applications. Your opinion was "<b id="op_owner"></b>". If you want to change your
                opinion, please select a new option below.
                <br/>
                <br/>
                <input type="radio" id="op_options_1" name="op_options" value="1">
                <label for="male"><h5>Not relevant</h5></label><br>
                <input type="radio" id="op_options_2" name="op_options" value="2">
                <label for="female"><h5>Relevant</h5></label><br>
                <input type="radio" id="op_options_3" name="op_options" value="3">
                <label for="other"><h5>Very relevant</h5></label>
                <br/>
                <br/>
                <h5>If you have changed your opinion, could you please justify your new choice? You can also make
                    any
                    other comment, as you wish.</h5>
                <br/>
                <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="op_text_option"></textarea>
                </div>
                <br/>

        </form>
        <hr/>

        <!-- End Operability -->

        <!-- Security -->
        <form action="javascript:sendData();"
              class="form-data-send" method="post">
            <h3>
                <b>Security</b>
                <i class="material-icons  mdl-list__item-avatar show-modal-security new_icon"
                   style="cursor: pointer">info</i>
            </h3>
            <br/>
            <h5>Notes:</h5>
            <h5>In the first round, for the <span id="se_name"></span> quality characteristic, <b
                        id="se_percent"></b>
                of experts have
                considered "<b id="se_text"></b>", <b id="se_percent_two"></b> considered "<b id="se_text_two"></b>",
                and <b id="se_percent_three"></b> considered "<b id="se_text_three"></b>" for the development of
                Smart City applications. Your opinion was "<b id="se_owner"></b>". If you want to change your
                opinion, please select a new option below.
                <br/>
                <br/>
                <input type="radio" id="se_options_1" name="se_options" value="1">
                <label for="male"><h5>Not relevant</h5></label><br>
                <input type="radio" id="se_options_2" name="se_options" value="2">
                <label for="female"><h5>Relevant</h5></label><br>
                <input type="radio" id="se_options_3" name="se_options" value="3">
                <label for="other"><h5>Very relevant</h5></label>
                <br/>
                <br/>
                <h5>If you have changed your opinion, could you please justify your new choice? You can also make
                    any
                    other comment, as you wish.</h5>
                <br/>
                <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="se_text_option"></textarea>
                </div>
                <br/>

        </form>
        <hr/>

        <!-- End Security -->


        <!-- Compatibility -->
        <form action="javascript:sendData();"
              class="form-data-send" method="post">
            <h3>
                <b>Compatibility</b>
                <i class="material-icons  mdl-list__item-avatar show-modal-compatibility new_icon"
                   style="cursor: pointer">info</i>
            </h3>
            <br/>
            <h5>Notes:</h5>
            <h5>In the first round, for the <span id="co_name"></span> quality characteristic, <b
                        id="co_percent"></b>
                of experts have
                considered "<b id="co_text"></b>", <b id="co_percent_two"></b> considered "<b id="co_text_two"></b>",
                and <b id="co_percent_three"></b> considered "<b id="co_text_three"></b>" for the development of
                Smart City applications. Your opinion was "<b id="co_owner"></b>". If you want to change your
                opinion, please select a new option below.
                <br/>
                <br/>
                <input type="radio" id="co_options_1" name="co_options" value="1">
                <label for="male"><h5>Not relevant</h5></label><br>
                <input type="radio" id="co_options_2" name="co_options" value="2">
                <label for="female"><h5>Relevant</h5></label><br>
                <input type="radio" id="co_options_3" name="co_options" value="3">
                <label for="other"><h5>Very relevant</h5></label>
                <br/>
                <br/>
                <h5>If you have changed your opinion, could you please justify your new choice? You can also make
                    any
                    other comment, as you wish.</h5>
                <br/>
                <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="co_text_option"></textarea>
                </div>
                <br/>

        </form>
        <hr/>

        <!-- End Compatibility -->

        <!-- Maintainability -->
        <form action="javascript:sendData();"
              class="form-data-send" method="post">
            <h3>
                <b>Maintainability</b>
                <i class="material-icons  mdl-list__item-avatar show-modal-maintainability new_icon"
                   style="cursor: pointer">info</i>
            </h3>
            <br/>
            <h5>Notes:</h5>
            <h5>In the first round, for the <span id="ma_name"></span> quality characteristic, <b
                        id="ma_percent"></b>
                of experts have
                considered "<b id="ma_text"></b>", <b id="ma_percent_two"></b> considered "<b id="ma_text_two"></b>",
                and <b id="ma_percent_three"></b> considered "<b id="ma_text_three"></b>" for the development of
                Smart City applications. Your opinion was "<b id="ma_owner"></b>". If you want to change your
                opinion, please select a new option below.
                <br/>
                <br/>
                <input type="radio" id="ma_options_1" name="ma_options" value="1">
                <label for="male"><h5>Not relevant</h5></label><br>
                <input type="radio" id="ma_options_2" name="ma_options" value="2">
                <label for="female"><h5>Relevant</h5></label><br>
                <input type="radio" id="ma_options_3" name="ma_options" value="3">
                <label for="other"><h5>Very relevant</h5></label>
                <br/>
                <br/>
                <h5>If you have changed your opinion, could you please justify your new choice? You can also make
                    any
                    other comment, as you wish.</h5>
                <br/>
                <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="ma_text_option"></textarea>
                </div>
                <br/>

        </form>
        <hr/>

        <!-- End Maintainability -->

        <!-- Transferability -->
        <form action="javascript:sendData();"
              class="form-data-send" method="post">
            <h3>
                <b>Transferability</b>
                <i class="material-icons  mdl-list__item-avatar show-modal-transferability new_icon"
                   style="cursor: pointer">info</i>
            </h3>
            <br/>
            <h5>Notes:</h5>
            <h5>In the first round, for the <span id="tf_name"></span> quality characteristic, <b
                        id="tf_percent"></b>
                of experts have
                considered "<b id="tf_text"></b>", <b id="tf_percent_two"></b> considered "<b id="tf_text_two"></b>",
                and <b id="tf_percent_three"></b> considered "<b id="tf_text_three"></b>" for the development of
                Smart City applications. Your opinion was "<b id="tf_owner"></b>". If you want to change your
                opinion, please select a new option below.
                <br/>
                <br/>
                <input type="radio" id="tf_options_1" name="tf_options" value="1">
                <label for="male"><h5>Not relevant</h5></label><br>
                <input type="radio" id="tf_options_2" name="tf_options" value="2">
                <label for="female"><h5>Relevant</h5></label><br>
                <input type="radio" id="tf_options_3" name="tf_options" value="3">
                <label for="other"><h5>Very relevant</h5></label>
                <br/>
                <br/>
                <h5>If you have changed your opinion, could you please justify your new choice? You can also make
                    any
                    other comment, as you wish.</h5>
                <br/>
                <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="tf_text_option"></textarea>
                </div>
                <br/>
        </form>
        <hr/>
        <!-- End Transferability -->

        <!-- EPI.C -->
        <form action="javascript:sendData();"
              class="form-data-send" method="post">
            <h3>
                <b>EPI.C</b>
                <i class="material-icons  mdl-list__item-avatar show-modal-epic new_icon"
                   style="cursor: pointer">info</i>
            </h3>
            <br/>
            <h5>Notes:</h5>
            <h5>In the first round, for the <span id="ec_name"></span> quality characteristic, <b
                        id="ec_percent"></b>
                of experts have
                considered "<b id="ec_text"></b>", <b id="ec_percent_two"></b> considered "<b id="ec_text_two"></b>",
                and <b id="ec_percent_three"></b> considered "<b id="ec_text_three"></b>" for the development of
                Smart City applications. Your opinion was "<b id="ec_owner"></b>". If you want to change your
                opinion, please select a new option below.
                <br/>
                <br/>
                <input type="radio" id="ec_options_1" name="ec_options" value="1">
                <label for="male"><h5>Not relevant</h5></label><br>
                <input type="radio" id="ec_options_2" name="ec_options" value="2">
                <label for="female"><h5>Relevant</h5></label><br>
                <input type="radio" id="ec_options_3" name="ec_options" value="3">
                <label for="other"><h5>Very relevant</h5></label>
                <br/>
                <br/>
                <h5>If you have changed your opinion, could you please justify your new choice? You can also make
                    any
                    other comment, as you wish.</h5>
                <br/>
                <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="ec_text_option"></textarea>
                </div>
                <br/>
        </form>
        <hr/>

        <!-- End EPI.C -->

        <!-- Context-awareness -->
        <form action="javascript:sendData();"
              class="form-data-send" method="post">
            <h3>
                <b>Context-awareness</b>
                <i class="material-icons  mdl-list__item-avatar show-modal-context new_icon"
                   style="cursor: pointer">info</i>
            </h3>
            <br/>
            <h5>Notes:</h5>
            <h5>In the first round, for the <span id="ca_name"></span> quality characteristic, <b
                        id="ca_percent"></b>
                of experts have
                considered "<b id="ca_text"></b>", <b id="ca_percent_two"></b> considered "<b id="ca_text_two"></b>",
                and <b id="ca_percent_three"></b> considered "<b id="ca_text_three"></b>" for the development of
                Smart City applications. Your opinion was "<b id="ca_owner"></b>". If you want to change your
                opinion, please select a new option below.
                <br/>
                <br/>
                <input type="radio" id="ca_options_1" name="ca_options" value="1">
                <label for="male"><h5>Not relevant</h5></label><br>
                <input type="radio" id="ca_options_2" name="ca_options" value="2">
                <label for="female"><h5>Relevant</h5></label><br>
                <input type="radio" id="ca_options_3" name="ca_options" value="3">
                <label for="other"><h5>Very relevant</h5></label>
                <br/>
                <br/>
                <h5>If you have changed your opinion, could you please justify your new choice? You can also make
                    any
                    other comment, as you wish.</h5>
                <br/>
                <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="ca_text_option"></textarea>
                </div>
                <br/>
        </form>
        <hr/>

        <!-- End Context-awareness -->

        <!-- Mobility -->
        <form action="javascript:sendData();"
              class="form-data-send" method="post">
            <h3>
                <b>Mobility</b>
                <i class="material-icons  mdl-list__item-avatar show-modal-mobility new_icon"
                   style="cursor: pointer">info</i>
            </h3>
            <br/>
            <br/>
            <h5>Notes:</h5>
            <h5>In the first round, for the <span id="mo_name"></span> quality characteristic, <b
                        id="mo_percent"></b>
                of experts have
                considered "<b id="mo_text"></b>", <b id="mo_percent_two"></b> considered "<b id="mo_text_two"></b>",
                and <b id="mo_percent_three"></b> considered "<b id="mo_text_three"></b>" for the development of
                Smart City applications. Your opinion was "<b id="mo_owner"></b>". If you want to change your
                opinion, please select a new option below.
                <br/>
                <br/>
                <input type="radio" id="mo_options_1" name="mo_options" value="1">
                <label for="male"><h5>Not relevant</h5></label><br>
                <input type="radio" id="mo_options_2" name="mo_options" value="2">
                <label for="female"><h5>Relevant</h5></label><br>
                <input type="radio" id="mo_options_3" name="mo_options" value="3">
                <label for="other"><h5>Very relevant</h5></label>
                <br/>
                <br/>
                <h5>If you have changed your opinion, could you please justify your new choice? You can also make
                    any
                    other comment, as you wish.</h5>
                <br/>
                <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="mo_text_option"></textarea>
                </div>
                <br/>
        </form>
        <hr/>

        <!-- End Mobility -->

        <!-- Attention -->
        <form action="javascript:sendData();"
              class="form-data-send" method="post">
            <h3>
                <b>Attention</b>
                <i class="material-icons  mdl-list__item-avatar show-modal-attention new_icon"
                   style="cursor: pointer">info</i>
            </h3>
            <br/>
            <h5>Notes:</h5>
            <h5>In the first round, for the <span id="at_name"></span> quality characteristic, <b
                        id="at_percent"></b>
                of experts have
                considered "<b id="at_text"></b>", <b id="at_percent_two"></b> considered "<b id="at_text_two"></b>",
                and <b id="at_percent_three"></b> considered "<b id="at_text_three"></b>" for the development of
                Smart City applications. Your opinion was "<b id="at_owner"></b>". If you want to change your
                opinion, please select a new option below.
                <br/>
                <br/>
                <input type="radio" id="at_options_1" name="at_options" value="1">
                <label for="male"><h5>Not relevant</h5></label><br>
                <input type="radio" id="at_options_2" name="at_options" value="2">
                <label for="female"><h5>Relevant</h5></label><br>
                <input type="radio" id="at_options_3" name="at_options" value="3">
                <label for="other"><h5>Very relevant</h5></label>
                <br/>
                <br/>
                <h5>If you have changed your opinion, could you please justify your new choice? You can also make
                    any
                    other comment, as you wish.</h5>
                <br/>
                <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="at_text_option"></textarea>
                </div>
                <br/>
        </form>
        <hr/>

        <!-- End Attention -->

        <!-- Calmness -->
        <form action="javascript:sendData();"
              class="form-data-send" method="post">
            <h3>
                <b>Calmness</b>
                <i class="material-icons  mdl-list__item-avatar show-modal-calmness new_icon"
                   style="cursor: pointer">info</i>
            </h3>
            <br/>
            <h5>Notes:</h5>
            <h5>In the first round, for the <span id="cm_name"></span> quality characteristic, <b
                        id="cm_percent"></b>
                of experts have
                considered "<b id="cm_text"></b>", <b id="cm_percent_two"></b> considered "<b id="cm_text_two"></b>",
                and <b id="cm_percent_three"></b> considered "<b id="cm_text_three"></b>" for the development of
                Smart City applications. Your opinion was "<b id="cm_owner"></b>". If you want to change your
                opinion, please select a new option below.
                <br/>
                <br/>
                <input type="radio" id="cm_options_1" name="cm_options" value="1">
                <label for="male"><h5>Not relevant</h5></label><br>
                <input type="radio" id="cm_options_2" name="cm_options" value="2">
                <label for="female"><h5>Relevant</h5></label><br>
                <input type="radio" id="cm_options_3" name="cm_options" value="3">
                <label for="other"><h5>Very relevant</h5></label>
                <br/>
                <br/>
                <h5>If you have changed your opinion, could you please justify your new choice? You can also make
                    any
                    other comment, as you wish.</h5>
                <br/>
                <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="cm_text_option"></textarea>
                </div>
                <br/>
        </form>
        <hr/>

        <!-- End Calmness -->

        <!-- Transparency. -->
        <form action="javascript:sendData();"
              class="form-data-send" method="post">
            <h3>
                <b>Transparency</b>
                <i class="material-icons  mdl-list__item-avatar show-modal-transparency new_icon"
                   style="cursor: pointer">info</i>
            </h3>
            <br/>
            <h5>Notes:</h5>
            <h5>In the first round, for the <span id="tr_name"></span> quality characteristic, <b
                        id="tr_percent"></b>
                of experts have
                considered "<b id="tr_text"></b>", <b id="tr_percent_two"></b> considered "<b id="tr_text_two"></b>",
                and <b id="tr_percent_three"></b> considered "<b id="tr_text_three"></b>" for the development of
                Smart City applications. Your opinion was "<b id="tr_owner"></b>". If you want to change your
                opinion, please select a new option below.
                <br/>
                <br/>
                <input type="radio" id="tr_options_1" name="tr_options" value="1">
                <label for="male"><h5>Not relevant</h5></label><br>
                <input type="radio" id="tr_options_2" name="tr_options" value="2">
                <label for="female"><h5>Relevant</h5></label><br>
                <input type="radio" id="tr_options_3" name="tr_options" value="3">
                <label for="other"><h5>Very relevant</h5></label>
                <br/>
                <br/>
                <h5>If you have changed your opinion, could you please justify your new choice? You can also make
                    any
                    other comment, as you wish.</h5>
                <br/>
                <div class="form-group" style="width: 60%">
                        <textarea class="form-control" rows="5" placeholder="Type here..."
                                  name="tr_text_option"></textarea>
                </div>
                <br/>
        </form>
        <hr/>

        <!-- End Calmness -->
        <br/>
        <br/>

    </div>
    <!--    Botão -->
    <div style="margin-top: 10px">
        <button class="mdl-button mdl-js-button
        mdl-button--raised mdl-js-ripple-effect
         mdl-button--accent btn-new"
                id="btn_send"
                style="float: right">
            Submit
        </button>
    </div>

    <div class="mdl-spinner mdl-js-spinner is-active load-tag" style="float: right"></div>
    <!--  End  Botão -->

    <!-- End page content -->
</div>

<!--Modal Funcional Suitability-->
<div class="modal fade" id="modal_dialog_functional" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    <b>Functional Suitability</b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important;">
                    The degree to which the software product provides functions that meet stated and
                    implied needs when the software is used under specified conditions [ISO/IEC 25010, 2011].
                </p>
                <p style="font-size: 1.3em!important;">
                    Functional suitability measures should be able to measure the degree to which a product or system
                    provides functions that meet stated and implied needs when used under specified conditions [ISO/IEC
                    25023,
                    2011].
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>


<!--End Modal Funcional Suitability-->

<!--Modal Reliability-->

<div class="modal fade" id="modal_dialog_reliability" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalReliability">
                    <b>Reliability</b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    The degree to which the software product can maintain a specified level of
                    performance when used under specified conditions [ISO/IEC 25010, 2011].
                </p>
                <p style="font-size: 1.3em!important;">
                    Reliability measures should be able to measure the degree to which a system, product or component
                    performs specified functions under specified conditions for a specified period of time [ISO/IEC
                    25023,
                    2011].
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<!--End Modal Reliability-->

<!--Modal Performance efficiency-->
<div class="modal fade" id="modal_dialog_efficiency" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalEfficiency">
                    <b>
                        Performance efficiency
                    </b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    The degree to which the software product provides appropriate performance, relative
                    to the amount of resources used, under stated conditions [ISO/IEC 25010, 2011].
                </p>
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    Performance efficiency measures should be able to measure the performance relative to the amount
                    of resources used under stated conditions. Resources can include other software products, the
                    software and hardware configuration of the system, and materials (e.g. print paper, storage media)
                    [ISO/IEC
                    25023, 2011].
                </p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<!--End Modal Performance efficiency-->

<!--Modal Operability -->

<div class="modal fade" id="modal_dialog_operability" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalOperability">
                    <b>
                        Operability
                    </b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    The degree to which the software product can be understood, learned, used and
                    attractive to the user, when used under specified conditions [ISO/IEC 25010, 2011].
                </p>
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    Operability measures should be able to measure the degree to which a product or system has
                    attributes that make it easy to operate and control [ISO/IEC 25023, 2011].
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<!--End Model Operability -->

<!--Modal Compatibility -->

<div class="modal fade" id="modal_dialog_compatibility" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCompatibility">
                    <b>
                        Compatibility
                    </b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    The ability of two or more software components to exchange information and/or to
                    perform their required functions while sharing the same hardware or software
                    environment [ISO/IEC 25010, 2011].
                </p>
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    Compatability measures should be able to measure the degree to which a product, system or
                    component can exchange information with other products, systems or components, and/or perform its
                    required functions, while sharing the same hardware or software environment [ISO/IEC 25023, 2011].
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<!--End Compatibility -->

<!--Modal Security -->

<div class="modal fade" id="modal_dialog_security" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalSecurity">
                    <b>
                        Security
                    </b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    The protection of system items from accidental or malicious access, use,
                    modification, destruction, or disclosure [ISO/IEC 25010, 2011].
                </p>
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    Security measures should be able to measure the degree to which a product or system protects
                    information and data so that persons or other products or systems have the degree of data access
                    appropriate to their types and levels of authorisation [ISO/IEC 25023, 2011].
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<!--End Model Security -->

<!--Modal Maintainability -->

<div class="modal fade" id="modal_dialog_maintainability" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalMaintainability">
                    <b>
                        Maintainability
                    </b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    The degree to which the software product can be modified. Modifications may
                    include corrections, improvements or adaptation of the software to changes in
                    environment, and in requirements and functional specifications [ISO/IEC 25010, 2011].
                </p>
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    Maintainability measures should be able to measure the degree of effectiveness and efficiency with
                    which a product or system can be modified by the intended maintainers [ISO/IEC 25023, 2011].
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<!--End Maintainability -->

<!--Modal Transferability -->

<div class="modal fade" id="modal_dialog_transferability" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalTransferability">
                    <b>
                        Transferability
                    </b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    The degree to which the software product can be transferred from one environment
                    to another [ISO/IEC 25010, 2011].
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<!--End Transferability -->

<!--Modal EPIC -->

<div class="modal fade" id="modal_dialog_epic" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalEpic">
                    <b>
                        EPIC
                    </b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    It is the system's ability to maintain compliance levels in smart pavement applications.
                </p>
                <br/>
                <h5><b>Reference</b></h5>
                <p style="font-size: 1em!important; width: 100%!important;">
                    Jesús Ramón Oviedo, Moisés Rodríguez, and Mario Piattini. 2015. Certification
                    of IPavement applications for smart cities a case study. In 2015 International
                    Conference on Evaluation of Novel Approaches to Software Engineering (ENASE).
                    IEEE, 244–249 136 (2018), 101–136.
                    <a href="https://ieeexplore.ieee.org/abstract/document/7320360" target="_blank">See here</a>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<!--End EPIC -->

<!--Modal Context-awareness -->

<div class="modal fade" id="modal_dialog_context" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalContext">
                    <b>
                        Context-awareness
                    </b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    The system’s capability of perceiving contextual information relating to the user, the system,
                    and the environment. With this information, the system dynamically and proactively adapts its
                    functionalities accordingly.
                </p>
                <br/>
                <h5><b>Reference</b></h5>
                <p style="font-size: 1em!important; width: 100%!important;">
                    Rainara Maia Carvalho, Rossana Maria de Castro Andrade, and Káthia Marçal
                    de Oliveira. 2018. AQUArIUM-A suite of software measures for HCI quality
                    evaluation of ubiquitous mobile applications. Journal of Systems and Software
                    136 (2018), 101–136. <a href="https://www.sciencedirect.com/science/article/pii/S0164121217302728"
                                            target="_blank">See here</a>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<!--End Context-awareness -->

<!--Modal Mobility -->

<div class="modal fade" id="modal_dialog_mobility" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalMobility">
                    <b>
                        Mobility
                    </b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    The system’s capability to provide users with continuous access to information resources
                    irrespective of
                    their location within the system’s boundaries.
                </p>
                <br/>
                <h5><b>Reference</b></h5>
                <p style="font-size: 1em!important; width: 100%!important;">
                    Rainara Maia Carvalho, Rossana Maria de Castro Andrade, and Káthia Marçal
                    de Oliveira. 2018. AQUArIUM-A suite of software measures for HCI quality
                    evaluation of ubiquitous mobile applications. Journal of Systems and Software
                    136 (2018), 101–136. <a href="https://www.sciencedirect.com/science/article/pii/S0164121217302728"
                                            target="_blank">See here</a>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<!--End Mobility -->

<!--Modal Attention -->

<div class="modal fade" id="modal_dialog_attention" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalAttention">
                    <b>
                        Attention
                    </b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    Refers to verifying if the user´s focus is on various mental and physical activities such as
                    walking,
                    driving or other real-world interactions rather than in technology [Garlan et al., 2002].
                </p>
                <br/>
                <h5><b>Reference</b></h5>
                <p style="font-size: 1em!important; width: 100%!important;">
                    Rainara Maia Carvalho, Rossana Maria de Castro Andrade, and Káthia Marçal
                    de Oliveira. 2018. AQUArIUM-A suite of software measures for HCI quality
                    evaluation of ubiquitous mobile applications. Journal of Systems and Software
                    136 (2018), 101–136. <a href="https://www.sciencedirect.com/science/article/pii/S0164121217302728"
                                            target="_blank">See here</a>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<!--End Attention -->

<!--Modal Calmness -->

<div class="modal fade" id="modal_dialog_calmness" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCalmness">
                    <b>
                        Calmness
                    </b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    Calmness means “free from distraction, excitement or disturbance”. A calm application
                    is one that is available anytime and anywhere, interacts with the user at the right time and
                    situation,
                    presents only relevant information, uses the periphery and the center of attention only when
                    necessary and
                    it is easy and natural to use [Weiser and Brown, 1997][Riekki et al., 2004],
                </p>
                <br/>
                <h4><b>Reference</b></h4>
                <p style="font-size: 1em!important; width: 100%!important;">
                    Rainara Maia Carvalho, Rossana Maria de Castro Andrade, and Káthia Marçal
                    de Oliveira. 2018. AQUArIUM-A suite of software measures for HCI quality
                    evaluation of ubiquitous mobile applications. Journal of Systems and Software
                    136 (2018), 101–136. <a href="https://www.sciencedirect.com/science/article/pii/S0164121217302728"
                                            target="_blank">See here</a>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<!--End Calmness -->

<!--Modal Transparency -->

<div class="modal fade" id="modal_dialog_transparency" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalTransparency">
                    <b>
                        Transparency
                    </b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="font-size: 1.3em!important; width: 100%!important;">
                    The ability to hide the system, so users may not be aware of it. This ability will happen if the
                    system
                    knows the user very well, and both his/her expectations and environment. Also, the ubiquitous system
                    should
                    hide its computing infrastructure in the environment, so the user does not realize that is
                    interacting with
                    a set of devices. This is about using various explicit inputs (which may not be perceived by users)
                    to make
                    automatically user actions.
                </p>
                <br/>
                <h4><b>Reference</b></h4>
                <p style="font-size: 1em!important; width: 100%!important;">
                    Rainara Maia Carvalho, Rossana Maria de Castro Andrade, and Káthia Marçal
                    de Oliveira. 2018. AQUArIUM-A suite of software measures for HCI quality
                    evaluation of ubiquitous mobile applications. Journal of Systems and Software
                    136 (2018), 101–136. <a href="https://www.sciencedirect.com/science/article/pii/S0164121217302728"
                                            target="_blank">See here</a>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<!--End Transparency -->

<!--Success -->

<div class="modal fade" id="modal_dialog_success" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle"
     align="justify" style="height: 50em!important; margin: 0 auto!important;width: 800px !important;"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 700px !important;">
        <div class="modal-content" style="width:640px !important;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalSuccess">
                    <b>
                        <p id="h_title"></p>
                    </b>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="texto_modal"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>


<!--End Success -->

<!-- W3.CSS Container -->
<div class="w3-light-grey w3-container w3-padding-32"
     style="margin-top:75px;padding-right:58px">
    <p class="w3-right">
        Powered by <a href="http://www.lsdi.ufma.br/" title="W3.CSS" target="_blank"
                      class="w3-hover-opacity">LSDi - UFMA</a>
    </p>
</div>

</body>
</html>
