<?php

include("connection.php");

$fs_options = $_POST['fs_options'];
$fs_text_option = $_POST['fs_text_option'];
$re_options = $_POST['re_options'];
$re_text_option = $_POST['re_text_option'];
$pe_options = $_POST['pe_options'];
$pe_text_option = $_POST['pe_text_option'];
$op_options = $_POST['op_options'];
$op_text_option = $_POST['op_text_option'];
$sec_options = $_POST['se_options'];
$sec_text_option = $_POST['se_text_option'];
$comp_options = $_POST['co_options'];
$comp_text_option = $_POST['co_text_option'];
$main_options = $_POST['ma_options'];
$main_text_option = $_POST['ma_text_option'];
$transf_options = $_POST['tf_options'];
$transf_text_option = $_POST['tf_text_option'];
$epeic_options = $_POST['ec_options'];
$epeic_text_option = $_POST['ec_text_option'];
$context_options = $_POST['ca_options'];
$context_text_option = $_POST['ca_text_option'];
$mob_options = $_POST['mo_options'];
$mob_text_option = $_POST['mo_text_option'];
$att_options = $_POST['at_options'];
$att_text_option = $_POST['at_text_option'];
$calm_options = $_POST['cm_options'];
$calm_text_option = $_POST['cm_text_option'];
$transp_options = $_POST['tr_options'];
$transp_text_option = $_POST['tr_text_option'];
$expert_id = htmlspecialchars($_POST['expertId']);


$query = "SELECT * FROM UF04_EXPERT WHERE UF04_COD_EXPERT = " . $expert_id;

$mail = '';
$result = pg_query($conn, $query) or die(error());
if (@pg_num_rows($result) == 0) {
    echo 0;
} else {
    $res = pg_fetch_assoc($result);
    $mail = $res['uf04_email'];
}

if ($fs_options == 'undefined') {
    echo 0;
    return;
} else {
    if ($fs_options == 0) {
        $fs_options = 1;
    } else if ($fs_options == 1) {
        $fs_options = 2;
    } else if ($fs_options) {
        $fs_options = 3;
    }
}
if (!$re_options) {
    echo 0;
    return;
} else {
    if ($re_options == 0) {
        $re_options = 1;
    } else if ($re_options == 1) {
        $re_options = 2;
    } else if ($re_options) {
        $re_options = 3;
    }
}

if ($pe_options == 'undefined') {
    echo 0;
    return;
} else {
    if ($pe_options == 0) {
        $pe_options = 1;
    } else if ($pe_options == 1) {
        $pe_options = 2;
    } else if ($pe_options) {
        $pe_options = 3;
    }
}
if ($op_options == 'undefined') {
    echo 0;
    return;
} else {
    if ($op_options == 0) {
        $op_options = 1;
    } else if ($op_options == 1) {
        $re_options = 2;
    } else if ($op_options) {
        $op_options = 3;
    }
}
if ($sec_options == 'undefined') {
    echo 0;
    return;
} else {
    if ($sec_options == 0) {
        $sec_options = 1;
    } else if ($sec_options == 1) {
        $sec_options = 2;
    } else if ($sec_options) {
        $sec_options = 3;
    }
}
if ($comp_options == 'undefined') {
    echo 0;
    return;
} else {
    if ($comp_options == 0) {
        $comp_options = 1;
    } else if ($comp_options == 1) {
        $comp_options = 2;
    } else if ($comp_options) {
        $comp_options = 3;
    }
}
if ($main_options == 'undefined') {
    echo 0;
    return;
} else {
    if ($main_options == 0) {
        $main_options = 1;
    } else if ($main_options == 1) {
        $main_options = 2;
    } else if ($main_options) {
        $main_options = 3;
    }
}
if ($transf_options == 'undefined') {
    echo 0;
    return;
} else {
    if ($transf_options == 0) {
        $transf_options = 1;
    } else if ($transf_options == 1) {
        $transf_options = 2;
    } else if ($transf_options) {
        $transf_options = 3;
    }
}
if ($epeic_options == 'undefined') {
    echo 0;
    return;
} else {
    if ($epeic_options == 0) {
        $epeic_options = 1;
    } else if ($epeic_options == 1) {
        $epeic_options = 2;
    } else if ($epeic_options) {
        $epeic_options = 3;
    }
}
if ($context_options == 'undefined') {
    echo 0;
    return;
} else {
    if ($context_options == 0) {
        $context_options = 1;
    } else if ($context_options == 1) {
        $context_options = 2;
    } else if ($context_options) {
        $context_options = 3;
    }
}
if ($mob_options == 'undefined') {
    echo 0;
    return;
} else {
    if ($mob_options == 0) {
        $mob_options = 1;
    } else if ($mob_options == 1) {
        $mob_options = 2;
    } else if ($mob_options) {
        $mob_options = 3;
    }
}
if ($att_options == 'undefined') {
    echo 0;
    return;
} else {
    if ($att_options == 0) {
        $att_options = 1;
    } else if ($att_options == 1) {
        $att_options = 2;
    } else if ($att_options) {
        $att_options = 3;
    }
}
if ($calm_options == 'undefined') {
    echo 0;
    return;
} else {
    if ($calm_options == 0) {
        $calm_options = 1;
    } else if ($calm_options == 1) {
        $calm_options = 2;
    } else if ($calm_options) {
        $calm_options = 3;
    }
}
if ($transp_options == 'undefined') {
    echo 0;
    return;
} else {
    if ($transp_options == 0) {
        $transp_options = 1;
    } else if ($transp_options == 1) {
        $transp_options = 2;
    } else if ($transp_options) {
        $transp_options = 3;
    }
}

//

//1,Functional Suitability

$q = "INSERT INTO UF09_ANSWER(FKUF09UF04_COD_EXPERT,
                        FKUF09UF01_COD_CHARACTERISTIC,
                        FKUF09UF03_COD_QUALIFIER,
                        UF09_DATE,
                        UF09_FINISH,
                        UF09_JUSTIFY,
                        UF09_EMAIL) VALUES ($expert_id, 1, " . $fs_options . " , current_timestamp, true, '" . $fs_text_option . "', '" . $mail . "')";
//2,Reliability
$q = $q . ", ($expert_id, 2, " . $re_options . ", current_timestamp, true,  '" . $re_text_option . "', '" . $mail . "')";
//3,Performance efficiency
$q = $q . ", ($expert_id, 3, " . $pe_options . ", current_timestamp, true,  '" . $pe_text_option . "', '" . $mail . "')";
//4,Operability
$q = $q . ", ($expert_id, 4, " . $op_options . ", current_timestamp, true,  '" . $op_text_option . "', '" . $mail . "')";
//5,Security
$q = $q . ", ($expert_id, 5, " . $sec_options . ", current_timestamp, true,  '" . $sec_text_option . "', '" . $mail . "')";
//6,Compatibility
$q = $q . ", ($expert_id, 6, " . $comp_options . ", current_timestamp, true,  '" . $comp_text_option . "', '" . $mail . "')";
//7,Maintainability
$q = $q . ", ($expert_id, 7, " . $main_options . ", current_timestamp, true,  '" . $main_text_option . "', '" . $mail . "')";
//8,Transferability
$q = $q . ", ($expert_id, 8, " . $transf_options . ", current_timestamp, true,  '" . $transf_text_option . "', '" . $mail . "')";
//9,EPI.C
$q = $q . ", ($expert_id, 9, " . $epeic_options . ", current_timestamp, true,  '" . $epeic_text_option . "', '" . $mail . "')";
//10,Context-awareness
$q = $q . ", ($expert_id, 10, " . $context_options . ", current_timestamp, true,  '" . $context_text_option . "', '" . $mail . "')";
//11,Mobility
$q = $q . ", ($expert_id, 11, " . $mob_options . ", current_timestamp, true,  '" . $mob_text_option . "', '" . $mail . "')";
//12,Attention
$q = $q . ", ($expert_id, 12, " . $att_options . ", current_timestamp, true,  '" . $att_text_option . "', '" . $mail . "')";
//13,Calmness
$q = $q . ", ($expert_id, 13, " . $calm_options . ", current_timestamp, true,  '" . $calm_text_option . "', '" . $mail . "')";
//14,Transparency
$q = $q . ", ($expert_id, 14, " . $transp_options . ", current_timestamp, true,  '" . $transp_text_option . "', '" . $mail . "')";

pg_query($conn, $q) or die(error());

echo 1;

?>
