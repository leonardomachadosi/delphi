<?php

include("connection.php");

$expert_id = htmlspecialchars($_POST['expertId']);

$query = "SELECT * FROM UF09_ANSWER WHERE FKUF09UF04_COD_EXPERT = $expert_id
 ORDER BY FKUF09UF01_COD_CHARACTERISTIC ASC;";

$result = pg_query($conn, $query) or die(error());

if (@pg_num_rows($result) == 0) {
    echo 0;
} else {
    $i = 0;
    while ($row = pg_fetch_array($result)){
        $res[$row['fkuf09uf01_cod_characteristic']] = $row;
        $i = $i + 1;
    }
    echo json_encode($res);
}

?>